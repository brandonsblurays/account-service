package com.brandonsblurays.account.rest.service;

import com.brandonsblurays.account.rest.error.AccountServiceError;
import com.brandonsblurays.account.rest.handler.AccountHandler;
import com.brandonsblurays.account.rest.resource.request.LoginRequest;
import com.brandonsblurays.account.rest.resource.response.TokenResponse;
import com.brandonsblurays.entity.account.Account;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private AccountHandler accountHandler;
    private BCryptPasswordEncoder encoder;

    public AccountService(AccountHandler accountHandler) {
        this.accountHandler = accountHandler;
        this.encoder = new BCryptPasswordEncoder();
    }

    public TokenResponse login(LoginRequest loginRequest) throws AccountServiceError {
        Account account = accountHandler.getAccount(loginRequest.getAccount());

        if(!encoder.matches(loginRequest.getPassword(), account.getPassword()))
            throw new AccountServiceError("Passwords do not match");

        return new TokenResponse(accountHandler.generateToken(account.getAccountId()));
    }

    public TokenResponse refresh(String accountId) {
        return new TokenResponse(accountHandler.generateToken(accountId));
    }
}
