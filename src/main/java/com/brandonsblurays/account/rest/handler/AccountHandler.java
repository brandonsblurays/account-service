package com.brandonsblurays.account.rest.handler;

import com.brandonsblurays.account.rest.error.AccountServiceError;
import com.brandonsblurays.account.rest.repository.AccountRepository;
import com.brandonsblurays.entity.account.Account;
import com.brandonsblurays.util.TokenUtil;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AccountHandler {

    private AccountRepository accountRepository;

    public AccountHandler(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account getAccount(String accountId) throws AccountServiceError {
        Optional<Account> accountResponse = accountRepository.findById(accountId);
        if(accountResponse.isPresent())
            return accountResponse.get();
        throw new AccountServiceError("Failed to find account");
    }

    public String generateToken(String accountId) {
        return TokenUtil.createJwtToken(accountId);
    }
}
