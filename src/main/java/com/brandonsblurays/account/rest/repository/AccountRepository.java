package com.brandonsblurays.account.rest.repository;

import com.brandonsblurays.entity.account.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<Account, String> {
}
