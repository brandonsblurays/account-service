package com.brandonsblurays.account.rest.controller;

import com.brandonsblurays.account.rest.error.AccountServiceError;
import com.brandonsblurays.account.rest.resource.request.LoginRequest;
import com.brandonsblurays.account.rest.resource.response.BasicResponse;
import com.brandonsblurays.account.rest.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/auth/account")
public class AccountController extends BaseController{
    private AccountService accountService;

    public AccountController(HttpServletRequest request, AccountService accountService) {
        super(request);
        this.accountService = accountService;
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginRequest request) {
        try {
            return ResponseEntity.ok(accountService.login(request));
        } catch(AccountServiceError e) {
            return ResponseEntity.badRequest().body(new BasicResponse(e.getErrorDetails()));
        }
    }

    @PostMapping("/refresh")
    public ResponseEntity refresh() {
        if(!isAuthed())
            return ResponseEntity.badRequest().body(new BasicResponse("Token is invalid"));
        return ResponseEntity.ok(accountService.refresh(getAccountId()));
    }
}
