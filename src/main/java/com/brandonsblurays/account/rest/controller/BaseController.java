package com.brandonsblurays.account.rest.controller;

import com.brandonsblurays.util.TokenUtil;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseController {
    private HttpServletRequest request;
    public BaseController(HttpServletRequest request) {
        this.request = request;
    }
    public boolean isAuthed() {
        String headerValue = TokenUtil.extractTokenFromRequest(request);
        if(headerValue == null || !TokenUtil.isTokenValid(headerValue))
            return false;
        return true;
    }

    public String getAccountId() {
        String headerValue = TokenUtil.extractTokenFromRequest(request);
        if(headerValue == null || !TokenUtil.isTokenValid(headerValue))
            return null;
        return TokenUtil.getAccountId(headerValue);
    }
}
