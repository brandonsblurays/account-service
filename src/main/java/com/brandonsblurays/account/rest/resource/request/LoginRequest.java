package com.brandonsblurays.account.rest.resource.request;

import lombok.Data;

@Data
public class LoginRequest {
    private String account;
    private String password;
}
