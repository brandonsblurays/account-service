package com.brandonsblurays.account.rest.error;

import lombok.Data;

@Data
public class AccountServiceError extends Exception {
    private String errorDetails;
    public AccountServiceError(String errorDetails) {
        this.errorDetails = errorDetails;
    }
}
